#include "xml_writer.h"
#include <assert.h>

int main(){
    IndentManager tabmgr = {0};

    fprintf(stdout, "indent manager tests\n");
    assert(xml_print_indent(stdout, &tabmgr) == 0);
    fprintf(stdout, "endl\n");
    increase_indent(&tabmgr);
    assert(xml_print_indent(stdout, &tabmgr) == 4);
    fprintf(stdout, "endl\n");

    fprintf(stdout, "\nField Tests\n");
    XMLField field1 = {"name", "value"};
    assert(xml_print_field(stdout, &field1) == 13);
    fprintf(stdout, "\n");


}