#include <stdio.h>
#include <string.h>
/**
 * @brief Simple xml file writing library
 * 
 * @author Gianni Absillis
 */
#ifndef XML_WRITER_H
#define XML_WRITER_H

/**
 * @brief indentation amount
 */
#define TAB_STOP 4

#define STR_LEN 80

// ===============
// = Indentation =
// ===============

/**
 * @brief Indent Manager
 * Manages the indentation levels of an XML file
 * Can be indented or closed
 */
typedef struct {
    int indentwidth;
} IndentManager;

/**
 * @brief Increase the indent of the indent manager by a tabstop
 * 
 * @param tabmgr the indent manager
 */
void increase_indent(IndentManager *tabmgr);

/**
 * @brief decrease the indent of the indent manager by a tabstop
 * 
 * @param tabmgr the indent manager
 */
void decrease_indent(IndentManager *tabmgr);

/**
 * @brief Overloaded print function for printing parts of xml files
 * Overloaded to print an IndentManager's current indent
 * @param fptr the file pointer
 * @param indent the indent manager
 * @return int the number of characters printed if successfull, negative if unsuccesful
 */
int xml_print_indent(FILE *fptr, IndentManager *indent);

// ==============
// = XML Fields =
// ==============

/**
 * @brief Name value pairs inside an xml tag
 */
typedef struct {
    char name[STR_LEN + 1];
    char value[STR_LEN + 1];
} XMLField;

/**
 * @brief Overloaded print function for printing parts of xml files
 * Overloaded to print an XML name value par as name="value"
 * prints with one trailing space
 * @param fptr the file pointer
 * @param field the XMLField to print
 * @return int the number of characters printed if successfull, negative if unsuccesful
 */
int xml_print_field(FILE *fptr, XMLField *field);

// ===============
// = Linked list =
// ===============
/**
 * @brief A node in the linked list
 */
typedef struct ListNodeStruct {
    /// @brief  the data stored in the node
    void *data; 
    /// @brief  the pointer to the next node
    struct ListNodeStruct *next;
} ListNode;


typedef struct {
    ListNode *start;
    ListNode *end;
    int size;
} ForwardLinkedList;

void initialize_empty_list(ForwardLinkedList *list);

int list_size(ForwardLinkedList *list);

ListNode *create_list_node(void *data);

ListNode *prepend_list(ForwardLinkedList *list, void *data);

ListNode *append_list(ForwardLinkedList *list, void *data);

// ============
// = XML Node =
// ============
/**
 * @brief An XML Tag that is the parent of other XMLNodes or XMLData
 * refers to the open and close tag combination
 * has any number of Fields
 */
typedef struct {
    char tagName[STR_LEN + 1];
    ForwardLinkedList fields;
    ForwardLinkedList childNodes;
} XMLNode;

/**
 * @brief creates an xml node with the given tag name,
 * an empty field list, and an empty child node list
 * 
 * @param tagName the tag name
 * @return the XMLNode allocated on the heap
*/
XMLNode *create_xml_node(const char *tagName);

/**
 * @brief print the xml node to the given File
 * also prints all child nodes
 * @param fptr the file pointer (use stdout for console)
 * @param node the node to print
 * @return int the number or characters printed (negative if fail)
 */
int xml_print_xml_node(FILE *fptr, IndentManager *indent, XMLNode *node);
#endif