#include "xml_writer.h"
#include <limits.h>
#include <stdlib.h>

int xml_print_indent(FILE *fptr, IndentManager *indent){
    const int sign_bit_mask = 1 << (sizeof(int) * CHAR_BIT - 1);
    int sign_int = 0;
    for(int i = 0; i < indent->indentwidth; i++){
        int pret = fprintf(fptr, " ");
        sign_int = sign_int | (pret & sign_bit_mask);
    }
    return sign_int | indent->indentwidth;
}

void increase_indent(IndentManager *indent){
    indent->indentwidth += TAB_STOP;
}

void decrease_indent(IndentManager *indent){
    indent->indentwidth -= TAB_STOP;
}

// ==============
// = XML Fields =
// ==============

int xml_print_field(FILE *fptr, XMLField *field){
    return fprintf(fptr, "%s=\"%s\" ", field->name, field->value);
}

// ===============
// = Linked list =
// ===============

void initialize_empty_list(ForwardLinkedList *list){
    list->start = NULL;
    list->end = NULL;
    list->size = 0;
}

int list_size(ForwardLinkedList *list){
    return list->size;;
}

ListNode *create_list_node(void *data){
    ListNode *newnode = (ListNode *) malloc(sizeof(ListNode));
    newnode->data = data;
    newnode->next = NULL;
}

ListNode *prepend_list(ForwardLinkedList *list, void *data){
    ListNode *newnode = (ListNode *) malloc(sizeof(ListNode));
    newnode->data = data;
    newnode->next = list->start;
    if(list->start == NULL){
        // empty list case: newnode is now the end as well
        list->end = newnode;
    }
    list->start = newnode;
    list->size++;
}

ListNode *append_list(ForwardLinkedList *list, void *data){
    ListNode *newnode = create_list_node(data);
    list->end->next = newnode;
    list->end = newnode;
    list->size++;
}

// ============
// = XML Node =
// ============

XMLNode *create_xml_node(const char *tagName){
    XMLNode *newNode = (XMLNode *) malloc(sizeof(XMLNode));
    strcpy(newNode->tagName, tagName);
    initialize_empty_list(&(newNode->fields));
    initialize_empty_list(&(newNode->childNodes));
    return newNode;
}

int xml_print_xml_node(FILE *fptr, IndentManager *indent, XMLNode *node){
    const int sign_bit_mask = 1 << (sizeof(int) * CHAR_BIT - 1);
    int sign_int = 0;

    int retsign = 0;
    int retcount = 0;
    int fprintf_val;

    // print the indent
    fprintf_val = xml_print_indent(fptr, indent);
    retcount += abs(fprintf_val);
    retsign |= fprintf_val & sign_bit_mask;

    // open the tag and print the tag name
    fprintf_val = fprintf(fptr, "<%s ", node->tagName); 
    retcount += abs(fprintf_val);
    retsign |= fprintf_val & sign_bit_mask;

    // print all the fields
    ListNode *it = node->fields.start;
    while(it != NULL){
        XMLField *field = (XMLField *) it->data;
        fprintf_val = xml_print_field(fptr, field);
        retcount += abs(fprintf_val);
        retsign |= fprintf_val & sign_bit_mask;  
        it = it->next;
    }

    // close the tag
    fprintf_val = fprintf(fptr, ">\n");
    retcount += abs(fprintf_val);
    retsign |= fprintf_val & sign_bit_mask;

    // TODO: child tags
    return retcount | retsign;
}